package com.example.paisajescuaterniones;

public class ColoresR {

    public ColoresR() {
    }

    /* Los colores x c/vértice (r,g,b,a) */
    byte maxColor = (byte) 255;
    public byte colorCafe[] = new byte[]{
// Frente
            (byte) 160, (byte) 80, (byte) 45, maxColor, // 4 0
            (byte) 160, (byte) 80, (byte) 45, maxColor, // 4 0
            (byte) 160, (byte) 80, (byte) 45, maxColor, // 4 0
            (byte) 160, (byte) 80, (byte) 45, maxColor, // 4 0
// Atrás
            (byte) 160, (byte) 80, (byte) 45, maxColor, // 4 0
            (byte) 160, (byte) 80, (byte) 45, maxColor, // 4 0
            (byte) 160, (byte) 80, (byte) 45, maxColor, // 4 0
            (byte) 160, (byte) 80, (byte) 45, maxColor, // 4 0
// Izquierda
            (byte) 139, (byte) 69, (byte) 19, maxColor, // 4 0
            (byte) 139, (byte) 69, (byte) 19, maxColor, // 4 0
            (byte) 139, (byte) 69, (byte) 19, maxColor, // 4 0
            (byte) 139, (byte) 69, (byte) 19, maxColor, // 4 0
// Derecha
            (byte) 139, (byte) 69, (byte) 19, maxColor, // 4 0
            (byte) 139, (byte) 69, (byte) 19, maxColor, // 4 0
            (byte) 139, (byte) 69, (byte) 19, maxColor, // 4 0
            (byte) 139, (byte) 69, (byte) 19, maxColor, // 4 0
// Abajo
            (byte) 139, (byte) 69, (byte) 19, maxColor, // 4 0
            (byte) 139, (byte) 69, (byte) 19, maxColor, // 4 0
            (byte) 139, (byte) 69, (byte) 19, maxColor, // 4 0
            (byte) 139, (byte) 69, (byte) 19, maxColor, // 4 0
// Arriba
            (byte) 139, (byte) 69, (byte) 19, maxColor, // 4 0
            (byte) 139, (byte) 69, (byte) 19, maxColor, // 4 0
            (byte) 139, (byte) 69, (byte) 19, maxColor, // 4 0
            (byte) 139, (byte) 69, (byte) 19, maxColor, // 4 0
    };


    public byte colorVerde[] = new byte[]{
// Frente
            (byte) 0, (byte) 100, (byte) 0, maxColor, // 4 0
            (byte) 0, (byte) 100, (byte) 0, maxColor, // 4 0
            (byte) 0, (byte) 100, (byte) 0, maxColor, // 4 0
            (byte) 0, (byte) 100, (byte) 0, maxColor, // 4 0
// Atrás
            (byte) 0, (byte) 100, (byte) 0, maxColor, // 4 0
            (byte) 0, (byte) 100, (byte) 0, maxColor, // 4 0
            (byte) 0, (byte) 100, (byte) 0, maxColor, // 4 0
            (byte) 0, (byte) 100, (byte) 0, maxColor, // 4 0
// Izquierda
            (byte) 34, (byte) 139, (byte) 34, maxColor, // 4 0
            (byte) 34, (byte) 139, (byte) 34, maxColor, // 4 0
            (byte) 34, (byte) 139, (byte) 34, maxColor, // 4 0
            (byte) 34, (byte) 139, (byte) 34, maxColor, // 4 0
// Derecha
            (byte) 34, (byte) 139, (byte) 34, maxColor, // 4 0
            (byte) 34, (byte) 139, (byte) 34, maxColor, // 4 0
            (byte) 34, (byte) 139, (byte) 34, maxColor, // 4 0
            (byte) 34, (byte) 139, (byte) 34, maxColor, // 4 0
// Abajo
            (byte) 34, (byte) 139, (byte) 34, maxColor, // 4 0
            (byte) 34, (byte) 139, (byte) 34, maxColor, // 4 0
            (byte) 34, (byte) 139, (byte) 34, maxColor, // 4 0
            (byte) 34, (byte) 139, (byte) 34, maxColor, // 4 0
// Arriba
            (byte) 34, (byte) 139, (byte) 34, maxColor, // 4 0
            (byte) 34, (byte) 139, (byte) 34, maxColor, // 4 0
            (byte) 34, (byte) 139, (byte) 34, maxColor, // 4 0
            (byte) 34, (byte) 139, (byte) 34, maxColor, // 4 0
    };
}
